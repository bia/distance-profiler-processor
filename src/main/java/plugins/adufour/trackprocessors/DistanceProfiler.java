package plugins.adufour.trackprocessors;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.vecmath.Point3d;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import icy.gui.dialog.ConfirmDialog;
import plugins.adufour.vars.gui.swing.SwingVarEditor;
import plugins.adufour.vars.lang.VarWorkbook;
import plugins.fab.trackmanager.PluginTrackManagerProcessor;
import plugins.fab.trackmanager.TrackGroup;
import plugins.fab.trackmanager.TrackSegment;
import plugins.nchenouard.spot.Detection;

public class DistanceProfiler extends PluginTrackManagerProcessor
{
    public DistanceProfiler()
    {
        setName("Relative distance between detections");
    }
    
    @Override
    public void Close()
    {
        
    }
    
    @Override
    public void Compute()
    {
        panel.setVisible(false);
        panel.removeAll();
        panel.setLayout(new BorderLayout());
        
        final VarWorkbook wb = new VarWorkbook("Distance profile", "Distance profile");
        SwingVarEditor<Workbook> viewer = (SwingVarEditor<Workbook>) wb.createVarViewer();
        panel.add(viewer.getEditorComponent(), BorderLayout.CENTER);
        
        Sheet sheet = wb.getValue().getSheetAt(0);
        int rowIndex = 0;
        
        Row headerRow = sheet.createRow(rowIndex++);
        headerRow.createCell(0).setCellValue("Group (A)");
        headerRow.createCell(1).setCellValue("Track (A)");
        headerRow.createCell(2).setCellValue("Group (B)");
        headerRow.createCell(3).setCellValue("Track (B)");
        headerRow.createCell(4).setCellValue("Frame");
        headerRow.createCell(5).setCellValue("Distance (pixels)");
        
        // System.out.println("Track Group (A)\tTrack ID (A)\tTrack Group (B)\tTrack ID (B)\tFrame\tDistance (pixels)");
        
        ArrayList<TrackGroup> trackGroups = trackPool.getTrackGroupList();
        
        for (int trackGroupIndexA = 0; trackGroupIndexA < trackGroups.size(); trackGroupIndexA++)
        {
            TrackGroup tgA = trackGroups.get(trackGroupIndexA);
            ArrayList<TrackSegment> trackSegmentsA = tgA.getTrackSegmentList();
            
            for (int trackGroupIndexB = trackGroupIndexA; trackGroupIndexB < trackGroups.size(); trackGroupIndexB++)
            {
                TrackGroup tgB = trackGroups.get(trackGroupIndexB);
                ArrayList<TrackSegment> trackSegmentsB = tgB.getTrackSegmentList();
                
                for (int trackSegmentIndexA = 0; trackSegmentIndexA < trackSegmentsA.size(); trackSegmentIndexA++)
                {
                    TrackSegment tsA = trackSegmentsA.get(trackSegmentIndexA);
                    int trackIDA = tgA.getTrackSegmentList().indexOf(tsA);
                    
                    for (int trackSegmentIndexB = trackSegmentIndexA; trackSegmentIndexB < trackSegmentsB.size(); trackSegmentIndexB++)
                    {
                        TrackSegment tsB = trackSegmentsB.get(trackSegmentIndexB);
                        
                        if (tsA == tsB) continue;
                        
                        int trackIDB = tgB.getTrackSegmentList().indexOf(tsB);
                        
                        // measure the distance between each detection set
                        for (Detection dA : tsA.getDetectionList())
                        {
                            Point3d pA = new Point3d(dA.getX(), dA.getY(), dA.getZ());
                            
                            for (Detection dB : tsB.getDetectionList())
                            {
                                if (dA.getT() != dB.getT()) continue;
                                
                                Point3d pB = new Point3d(dB.getX(), dB.getY(), dB.getZ());
                                
                                Row row = sheet.createRow(rowIndex++);
                                row.createCell(0).setCellValue(tgA.getDescription());
                                row.createCell(1).setCellValue("" + trackIDA);
                                row.createCell(2).setCellValue(tgB.getDescription());
                                row.createCell(3).setCellValue("" + trackIDB);
                                row.createCell(4).setCellValue("" + dA.getT());
                                row.createCell(5).setCellValue(pA.distance(pB));
                                // System.out.println(tgA.getDescription() + "\t" + trackIDA + "\t"
                                // + tgB.getDescription() + "\t" + trackIDB + "\t" + dA.getT() +
                                // "\t" + pA.distance(pB));
                            }
                        }
                    }
                }
            }
        }
        // notify the viewer that the value has changed
        viewer.valueChanged(wb, null, wb.getValue());
        
        JPanel options = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton bExport = new JButton("Export to Excel");
        bExport.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                JFileChooser f = new JFileChooser();
                if (f.showOpenDialog(getPanel().getParent()) != JFileChooser.APPROVE_OPTION) return;
                
                File file = f.getSelectedFile();
                if (!file.getPath().endsWith(".xls") && !file.getPath().endsWith(".xlsx")) file = new File(file.getPath() + ".xls");
                
                if (!file.exists() || ConfirmDialog.confirm("Overwrite " + file.getPath() + "?"))
                {
                    try
                    {
                        wb.getValue().write(new FileOutputStream(file, false));
                    }
                    catch (Exception e1)
                    {
                        throw new RuntimeException(e1);
                    }
                }
            }
        });
        options.add(bExport);
        
        panel.add(options, BorderLayout.NORTH);
        
        panel.setVisible(true);
    }
    
    @Override
    public void displaySequenceChanged()
    {
    }
}
